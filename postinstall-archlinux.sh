#!/bin/bash

# Este script instala software y copia archivos de configuración para la post
# instalación de Archlinux de acuerdo a mi configuración personal.
# El presente script es de libre uso y cuenta con una licencia GNU GPL V3

echo 'Deben instalarse algunos paquetes ¿Continuar? [s/n]'
echo 'La lista de paquetes que se instalarán se encuentra en el archivo dependences.txt'
read option

if [ $option != 's' ]; then
    echo 'Puede que necesites instalar algunos paquetes manualmente.'
else
    sudo pacman -S --needed - < dependences.txt
fi

echo '¿Quieres instalar zsh y OhMyZsh? [s/n]'
echo ''
read ohmyzshOption

if [ $ohmyzshOption != 's' ]; then
    echo 'Se continúa con la configuración...'
else
    sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
fi

git clone https://gitlab.com/jona3717/my-dot-files/

echo 'Copiando archivos'
cd my-dot-files

if [ $ohmyzshOption == 's' ]; then
    cp .zshrc ~/
fi

echo '¿Copiar configuración de alacritty? [s/n]'
read option

if [ $option == 's' ]; then
    cp .alacritty.yml ~/
    mkdir ~/.scripts
    cp pacman.sh ~/.scripts/
fi

echo '¿Copiar configuración de Kitty? [s/n]'
read option

if [ $option == 's' ]; then
    cp -r .config/kitty ~/.config/
    mkdir ~/.scripts
    cp pacman.sh ~/.scripts/
fi

echo '¿Copiar configuración de Neovim? [s/n]'
read option

if [ $option == 's' ]; then
    cp -r .config/nvim ~/.config/
    cp -r .local ~/
    cp -r .vim ~/
    cp .vimrc ~/
fi

echo '¿Copiar configuración de i3? [s/n]'
read option

if [ $option == 's' ]; then
    cp -r .config/i3 ~/.config/
    sudo cp -r etc /etc
    sudo cp plasma-i3.desktop /usr/share/xsessions/
    sudo cp plasma-i3.sh /bin
    sudo cp plasma-i3.sh /usr/bin/
fi

cd

echo 'Terminó la configuración.'
echo 'Para continuar, debes abrir nvim e instalar los plugins con el comando: PlugInstall'

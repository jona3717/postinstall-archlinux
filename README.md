Script de Post Instalación para Archlinux
===========================================

Este script está escrito en bash, permite instalar una lista de paquetes y configurar algunas aplicaciones que se mencionan a continuación:

Lista de Paquetes:
------------------

- zsh
- wget
- git
- npm
- go
- neovim
- cmake
- alacritty
- firefox
- firefox-i18n-es-es
- telegram-desktop
- obs-studio
- kdenlive
- simplescreenrecorder
- audacity
- steam
- virtualbox
- vlc
- gwenview
- okular
- dolphin
- neofetch
- htop
- konsole
- ranger
- libreoffice-fresh
- libreoffice-fresh-es
- htop
- ark
- unrar
- unzip
- spectacle
- sweeper
- kcalc
- redshift
- i3-gaps
- i3blocks
- xorg-xinput
- xcompmgr
- fortune-mod
- cowsay

Esta lista puede ser modificada para agregar o quitar los paquetes que se quieran, editando el archivo "dependences.txt"

Las configuraciones que puede realizar este script son:
-------------------------------------------------------

- Instalar OhMyZsh para personalizar la terminal y cargar el tema fletcherm
- Terminal Alacritty
- Neovim
- Plasma con el gestor de ventanas i3 Gaps
- YouCompleteMe para Neovim

Para realizar la configuración basta con dar permisos y ejecutar el script:

`chmod +x postinstall-archlinux.sh
./postinstall-archlinux.sh`

El presente script cuenta con licencia GNU GPL V3

Capturas de Pantalla:
---------------------

![Plasma i3](plasma-i3.png)

![Alacritty](alacritty.png)

![Nvim](nvim.png)
